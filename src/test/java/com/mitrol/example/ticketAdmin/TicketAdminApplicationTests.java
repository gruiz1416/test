package com.mitrol.example.ticketAdmin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.model.TicketStatus;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.io.IOException;

@SpringBootTest(classes = TicketAdminApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
		locations = "classpath:application-integrationtest.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TicketAdminApplicationTests {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	@Test
	@Order(1)
	void fetchTicketTest() throws Exception {
		mvc.perform(get("/ticket")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", is(Matchers.empty())));
	}


	@Test
	@Order(2)
	void createTicketTest() throws Exception {
		String ticketNameTest = "Test 1";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		mvc.perform(get("/ticket")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].name", is(ticketNameTest)));

	}
	
	@Test
	@Order(3)
	void getTicketTest() throws Exception {
		String ticketNameTest = "Test 2";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		
		/*
		 * Genero un nuevo ticket
		 * */
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));
		
		/*
		 * Obtengo el nuevo ticket y verifico el nombre
		 * */
		mvc.perform(get("/ticket/2")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

	}
	
	@Test
	@Order(4)
	void updateTicketTest() throws Exception {
		String ticketNameTest = "Test 3";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		
		/*
		 * Genero un nuevo ticket
		 * */
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		/*
		 * Actualizo el status del ticket y verifico recibir un OK
		 * */
		mvc.perform(put("/ticket/3")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		/*
		 * Obtengo el nuevo ticket y verifico que haya avanzado al siguiente estado
		 * */
		mvc.perform(get("/ticket/3")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is("WORKING")));

	}
	
	@Test
	@Order(5)
	void deleteTicketTest() throws Exception {
		String ticketNameTest = "Test 4";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		/*
		 * Genero un nuevo ticket
		 * */
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));
		
		/*
		 * Actualizo al siguiente estado y verifico OK
		 * */
		mvc.perform(put("/ticket/4")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

		/*
		 * Intento eliminar, pero no esta en estado DONE
		 * Verifico NOT_ACEPTABLE
		 * */
		mvc.perform(delete("/ticket/4")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
		
		/*
		 * Actualizo nuevamente para avanzar a DONE
		 * */
		mvc.perform(put("/ticket/4")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		/*
		 * Intento eliminar nuevamente y verifico OK
		 * */
		mvc.perform(delete("/ticket/4")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}
	
	@Test
	@Order(6)
	void getNotFinishedTicketTest() throws Exception {
		String ticketNameTest = "Test 5";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		/*
		 * Genero un nuevo ticket
		 * */
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		/*
		 * Actualizo a WORKING
		 * */
		mvc.perform(put("/ticket/5")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

		/*
		 * Actualizo a DONE
		 * */
		mvc.perform(put("/ticket/5")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		/*
		 * Verifico que haya 4 tickets, estan incluidos los de los test anteriores, que 
		 * no se encuentran en DONE, el unico actualizado a DONE fue eliminado en el test anterior
		 * */
		mvc.perform(get("/ticket/")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
				.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", is(Matchers.hasSize(4))));
		
		/*
		 * Traigo los no finializados, y ya que de los 4, solo uno se encuentra en DONE
		 * verifico que haya 3 resultados
		 * */
		mvc.perform(get("/ticket/notFinished")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
				.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", is(Matchers.hasSize(3))));

	}
	
	@Test
	@Order(7)
	void updateNonExistingTicketTest() throws Exception {
		/*
		 * Verifico respuesta de no encontrado al actualizar un ticket que no existe
		 * */
		mvc.perform(put("/ticket/10")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	@Test
	@Order(8)
	void deleteNonExistingTicketTest() throws Exception {
		/*
		 * Verifico respuesta de no encontrado al eliminar un ticket que no existe
		 * */
		mvc.perform(delete("/ticket/10")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	@Test
	@Order(9)
	void failToUpdateTicketTest() throws Exception {
		String ticketNameTest = "Test 3";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		
		/*
		 * Genero un nuevo ticket
		 * */
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		/*
		 * Actualizo el status del ticket a WORKING y verifico recibir un OK
		 * */
		mvc.perform(put("/ticket/6")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		/*
		 * Actualizo el status del ticket a DONE y verifico recibir un OK
		 * */
		mvc.perform(put("/ticket/6")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		/*
		 * Intento actualizar nuevamente el status, pero al ya estar en DONE
		 * verifico recibir un NOT_ACEPTABLE
		 * */
		mvc.perform(put("/ticket/6")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
		
		
	}


}
