package com.mitrol.example.ticketAdmin.controller;

import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TicketController {

    @Autowired
    private TicketService service;

    @GetMapping("/ticket")
    public List<TicketDto> findAll() {
        return service.findAll();
    }

    @PostMapping("/ticket")
    public TicketDto create(@RequestBody TicketDto ticket) {
        return service.create(ticket);
    }
    
    @GetMapping("/ticket/{id}")
    public ResponseEntity getById(@PathVariable Integer id) {
    	
    	return service.getById(id);
        
    }
    
    @PutMapping("/ticket/{id}")
    public ResponseEntity updateTicket(@PathVariable Integer id) {
    	
    	return service.updateTicket(id);
    	
    }
    
    @DeleteMapping("/ticket/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
    	
    	return service.deleteTicket(id);
    	
    }
    
    @GetMapping("/ticket/notFinished")
    public List<TicketDto> getNotFinishedTickets() {
    	
    	return service.getUnfinishedTickets();
 
    }


}
