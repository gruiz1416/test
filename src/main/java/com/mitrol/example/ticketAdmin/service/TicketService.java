package com.mitrol.example.ticketAdmin.service;

import com.mitrol.example.ticketAdmin.entity.Ticket;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.model.TicketStatus;
import com.mitrol.example.ticketAdmin.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TicketService {

    @Autowired
    private TicketRepository repository;

    private TicketDto toDto(Ticket entity){
        TicketDto dto = new TicketDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setStatus(entity.getStatus());
        return dto;
    }

    private Ticket toEntity(TicketDto dto){
        Ticket entity = new Ticket();
        entity.setName(dto.getName());
        entity.setStatus(dto.getStatus());
        return entity;
    }


    public List<TicketDto> findAll(){
        List<Ticket> entities = repository.findAll();
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public TicketDto create(TicketDto ticket){
        Ticket entity = toEntity(ticket);
        repository.save(entity);
        return toDto(entity);
    }
    
    public ResponseEntity getById(Integer id) {
    	
    	TicketDto ticketDto = null;
    	
    	try {
    		
    		Ticket ticket = repository.getOne(id);
    		return new ResponseEntity<>(toDto(ticket), HttpStatus.OK);
    		
    	} catch (Exception e) {
    		
    		return new ResponseEntity<>("No se ha encontrado un ticket con id: " + id, HttpStatus.NOT_FOUND);
    	
    	}
    	
    }

    public ResponseEntity updateTicket(Integer id) {
    	
    	try {
    		
    		Ticket ticket = repository.getOne(id);
    		if (TicketStatus.PENDING.equals(ticket.getStatus())) {
    			ticket.setStatus(TicketStatus.WORKING);
    			repository.save(ticket);
    			return new ResponseEntity<>("Ticket actualizado a: " + TicketStatus.WORKING, HttpStatus.OK);
    		} else if (TicketStatus.WORKING.equals(ticket.getStatus())) {
    			ticket.setStatus(TicketStatus.DONE);
    			repository.save(ticket);
    			return new ResponseEntity<>("Ticket actualizado a: " + TicketStatus.DONE, HttpStatus.OK);
    		} else {
    			return new ResponseEntity<>("El ticket ya se encuentra en estado " + TicketStatus.DONE, HttpStatus.NOT_ACCEPTABLE);
    		}
    		
    	} catch (Exception e) {
    		
    		return new ResponseEntity<>("No se encontr� un ticket con id: " + id, HttpStatus.NOT_FOUND);
    		
    	}
    	
    }
    
    public ResponseEntity deleteTicket (Integer id) {
    	
    	try {
    		
    		Ticket ticket = repository.getOne(id);
    		if (TicketStatus.DONE.equals(ticket.getStatus())) {
    			repository.deleteById(id);
    			return new ResponseEntity<>("El ticket con id " + id + " fue eliminado correctamente.", HttpStatus.OK);
    		} else {
    			return new ResponseEntity<>("El ticket con id " + id + " no puede ser eliminado.", HttpStatus.NOT_ACCEPTABLE);
    		}
    		
    	} catch (Exception e) {
    		
    		return new ResponseEntity<>("No se encontr� un ticket con id: " + id, HttpStatus.NOT_FOUND);
    		
    	}
    	
    }
    
    public List<TicketDto> getUnfinishedTickets() {
    	
    	List<Ticket> list = repository.findAll();
    	List<TicketDto> filteredList = new ArrayList<>();
    	
    	for (Ticket ticket : list) {
    		if (!TicketStatus.DONE.equals(ticket.getStatus())) {
    			filteredList.add(toDto(ticket));
    		}
    	}
    	
    	return filteredList;
    
    }


}
